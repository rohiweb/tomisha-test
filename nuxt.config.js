const opn = require('opn');

export default {
  // Disable server-side rendering: https://go.nuxtjs.dev/ssr-mode
  ssr: false,

  // Global page headers: https://go.nuxtjs.dev/config-head
  head: {
    title: 'tomisha-test',
    htmlAttrs: {
      lang: 'en',
    },
    meta: [
      { charset: 'utf-8' },
      { name: 'viewport', content: 'width=device-width, initial-scale=1' },
      { hid: 'description', name: 'description', content: '' },
      { name: 'format-detection', content: 'telephone=no' },
    ],
    link: [
      { rel: 'icon', type: 'image/x-icon', href: '/favicon.ico' },
      { href: 'http://fonts.googleapis.com/css?family=Lato:400,700', rel: 'stylesheet', type: 'text/css'}
    ],
  },

  // Global CSS: https://go.nuxtjs.dev/config-css
  css: [],

  // Plugins to run before rendering page: https://go.nuxtjs.dev/config-plugins
  plugins: [],

  // Auto import components: https://go.nuxtjs.dev/config-components
  components: true,

  // Modules for dev and build (recommended): https://go.nuxtjs.dev/config-modules
  buildModules: [
    // https://go.nuxtjs.dev/typescript
    '@nuxt/typescript-build',
    // https://go.nuxtjs.dev/tailwindcss
    '@nuxtjs/tailwindcss',
  ],

  // Modules: https://go.nuxtjs.dev/config-modules
  modules: [],

  // Build Configuration: https://go.nuxtjs.dev/config-build
  build: {},

  tailwindcss: {
    config: {
      mode: 'jit',
      theme: {
        fontFamily: {
          sans: ['Lato', 'sans-serif'],
        },
        extend: {
          colors: {
            'branding-primary': '#319795',
            'branding-secondary': '#3182CE',
            'branding-primary-light': '#B2F5EA',
            'branding-secondary-light': '#BEE3F8',
          },
          boxShadow: {
            'md-t': '0px -1px 3px 0px rgba(0,0,0,0.2)',
            'hover': 'inset 0 0 140px 140px rgb(0,0,0,0.3)',
            'active': 'inset 0 0 140px 140px rgb(0,0,0,0.4)',
          }
        }
      }
    }
  },

  hooks: {
    listen(server, { host, port }) {
      opn(`http://${host}:${port}`);
    }
  }
}
